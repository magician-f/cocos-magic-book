/**
 * @name 这是一个实例，除了静态的和类，所有其它接口请从此单例开始
 * https://gitee.com/magician-f/cocos-magic-book
 */
export const mb = new class {

    /**
     * 初始化完成以后才能使用
     * @param conf 
     * @param func_cb 
     */
    init(func_cb?: () => void) {
        //挂载到全局方便debug
        window["__mb"] = this;
        func_cb && func_cb();
    }

};
export { default as mb_config } from "./config/mb_config";
export { mb_audio, mb_audio_const } from "./system/audio/mb_audio";
export { default as mb_base_cp } from "./base/mb_base_cp";