export default class mb_config {
    /**
     * 是否启用debug模式，会有更多日志输出
     */
    static is_debug: boolean = false;
};